let numberEntries = new Set();  

numberEntries.add(null);
numberEntries.add('null');
numberEntries.add(2);

console.log(numberEntries.has(2));
numberEntries.delete('null');

for(let item of numberEntries){
    console.log(item);
}

numberEntries.clear();
console.log(numberEntries.size);