import Promise, { resolve } from 'promise';
import { user } from '../model/user'; 
import * as fs from 'fs';

export function newUser(userObject : any){
    return new Promise(function(resolve,reject){
        try{
            const fileData = fs.readFileSync('../userDetails.json','utf-8');
        let arrayOfObjects = JSON.parse(fileData);
        arrayOfObjects.userData.push(userObject);

        fs.writeFileSync('../userDetails.json',JSON.stringify(arrayOfObjects))
        resolve(userObject);
        }catch(err) {
            reject(console.error(err))
        }   
    })
}