import { scheduleTask } from './allmodules/scheduler';
import {question}  from 'readline-sync';
import  StringClass  from './allmodules/stringHandler';
import  ObjectClass  from './allmodules/objectHandler';
import  ArrayClass  from './allmodules/arrayHandler';

export function startTask():void{
    console.log('in start task');

    scheduleTask('Assignment 1',()=>{
      console.log('abc');
    });
}


function menuDriven() {
   
    const options: string = question(
      "Enter options\n press 1 = stringHandler \n press 2 = arrayHandler \n press 3 = objectHandler \n press 4 = scheduler \n press 5 = Exit\n "
    );
    
      switch(options){
          case '1':
            return new StringClass();
          case '2':
             return   new ArrayClass();
          case '3':
              return new ObjectClass();
          case '4':
            return startTask();
          default:
               console.log('exited\n');
               break;
              
      }
    }
  
  menuDriven();