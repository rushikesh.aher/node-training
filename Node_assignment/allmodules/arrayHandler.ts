export default class ArrayClass {
 static trainees: string[] = [];

  constructor() {
    // ArrayClass.newTrainees("rushi");
    // ArrayClass.newTrainees("ra");
    // ArrayClass.addAtTop("Swapnil");
    // ArrayClass.addAtTop("mayur");
    // ArrayClass.removeTrainee();
  }

  static newTrainees(newJoinser: string): string[] {
    ArrayClass.trainees.push(newJoinser);
    return ArrayClass.trainees;
  }

  static noOfTrainees(): number {
    return ArrayClass.trainees.length;
  }

  static addAtTop(trainee: string): any {
    var tempArray: string[] = [];

    if (!ArrayClass.trainees.includes(trainee)) {
      tempArray.push(trainee);
      ArrayClass.trainees.forEach((item) => {
        tempArray.push(item);
      });
      ArrayClass.trainees = tempArray;
      console.log(ArrayClass.trainees);
      return ArrayClass.trainees;
    } else {
      return "Trainee already exist";
    }
  }

  static addTrainee(trainee: string): any {
    ArrayClass.trainees.push(trainee);
    return ArrayClass.trainees;
  }

  static removeTrainee(): string[] {
      ArrayClass.trainees.splice(2, 2);
    console.log(ArrayClass.trainees);
    return ArrayClass.trainees;
  }

  static sortTrainee() : string[]{
    console.log(ArrayClass.trainees.sort());
    return ArrayClass.trainees.sort();
  }
}
